<?php get_header(); ?>

<section class="page-contents">
	<div class="container">
		
		<div class="row">
			<div class="col-md-9 blog-page">

				<h1 class="page-title search-title"><?php 
					if ( is_day() ) { printf( __( 'Daily Archives: %s', 'awesome' ), get_the_time( get_option( 'date_format' ) ) ); }
					elseif ( is_month() ) { printf( __( 'Monthly Archives: %s', 'awesome' ), get_the_time( 'F Y' ) ); }
					elseif ( is_year() ) { printf( __( 'Yearly Archives: %s', 'awesome' ), get_the_time( 'Y' ) ); }
					else { _e( 'Archives', 'awesome' ); }
					?></h1>

				<?php
					while(have_posts()) :
						the_post();
						$post_thumbnail = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
						$content = get_the_excerpt();
				?>

				<div class="blog-entry">
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php echo awesome_post_details(); ?>

					<div class="row">
						<div class="col-md-3">
							<?php if(has_post_thumbnail()) : ?>
								<img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo $post_thumbnail; ?>&w=200&h=200" alt="<?php the_title(); ?> Thumbnail" class="img-responsive">
							<?php else : ?>
								<img src="http://placehold.it/200/62b842/ffffff" class="img-responsive" alt="No thumbnail" title="Thumbnail is not set">
							<?php endif; ?>
						</div>
						<div class="col-md-9">
							<p><?php echo $content; ?></p>
							<a href="<?php the_permalink(); ?>" class="awesome-btn">read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>

				<?php endwhile; ?>

			</div>
			<div class="col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>

	</div>	
</section>

<?php get_footer(); ?>