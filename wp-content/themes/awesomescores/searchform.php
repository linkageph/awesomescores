<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<input type="search" class="search-field form-control" placeholder="Search …" value="" name="s" title="Search for:" />
	</div>
	<div class="form-group">
		<input type="submit" class="search-submit form-control" value="Search" />	
	</div>
</form>