<footer>
    <div class="footer-upper">
        <div class="container">
            
            <div class="row">
                <div class="col-md-3 text-center footer-logo">
                    <a href="<?php echo site_url(); ?>"><img src="<?php echo get_image('logo.png'); ?>"></a>
                    <p>Registraion for classes is continual. Sign up early to assure your spot! If the class you want is sold out, look at a future class. My classes are scheduled to coincide with ACT and SAT tests.</p>
                </div>
                <div class="col-md-9 footer-content">
                    <h1>Top Ten Reasongs to Take Awesome Scores Prep Classes.</h1>
                    <div class="row">
                        <div class="col-md-4">
                            <ul class="list-unstyled">
                                <li>10. 8 hours of live help for $399</li>
                                <li>9. Can you say Scholarship?</li>
                                <li>8. It's affordable!</li>
                                <li>7. Get into the school of your choice.</li>
                                <li>6. Bragging rights.</li>
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <ul class="list-unstyled">
                                <li>5. Learn test taking strategies.</li>
                                <li>4. Increased confidence!</li>
                                <li>3. Easy because you are comfortable in your own home.</li>
                                <li>2. See all problems explained.</li>
                                <li>1. Do you really want to take and pay for developmental classes for no credit?</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="lower-footer">
        <div class="container">
            
            <div class="row">
                <div class="col-md-6">
        
                    <?php

                        $defaults = array(
                            'theme_location'  => 'secondary',
                            'container'       => '',
                            'menu_class'      => 'list-inline',
                        );

                        wp_nav_menu( $defaults );

                    ?>   

                </div>
                <dic class="col-md-6">
                    <p class="text-right">Copyright &copy; 2013 Awesome Scores. Website by: <a href="http://customadesign.com" target="_blank"><img src="<?php echo get_image('customalogo.png'); ?>"></a></p>
                </dic>
            </div>

        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>