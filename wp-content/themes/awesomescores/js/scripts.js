jQuery(function($){

	$(window).load(function(){
		$('.flexslider').flexslider({
			directionNav: false,
		});
	});

});

jQuery(document).ready(function($){

	// deletes all empty <p> tags
	$("p").filter( function() {
	    return $.trim($(this).html()) == '';
	}).remove()

	// give all submit buttons an awesome-btn class
	$('input[type="submit"]').addClass('awesome-btn');

});
