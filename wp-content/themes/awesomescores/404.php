<?php get_header(); ?>

<section class="page-contents">
	<div class="container">
		
		<div class="row">
			<div class="col-md-9 page-content">

				<h1 class="page-title"><?php _e('Not Found', 'awesome'); ?></h1>

				<p>It looks like the page you're looking for has been removed or it doesn't exist. Try another search below.</p>
				<?php get_search_form(); ?>

			</div>
			<div class="col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>

	</div>	
</section>

<?php get_footer(); ?>