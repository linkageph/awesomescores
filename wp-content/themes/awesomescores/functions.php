<?php
// Helper Functions
function get_image($image)
{
	return get_template_directory_uri() . '/images/' . $image;
}

function get_awesome_template($template_name)
{
	return get_template_part('templates/' . $template_name);
}

function page_excerpt()
{
	$content = get_post_meta(get_the_ID(), 'page_excerpt', true);
	return $content;
}

// Main Functions
// add_filter( 'show_admin_bar', '__return_false' );

add_action( 'after_setup_theme', 'awesome_setup' );
function awesome_setup()
{
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );

	register_nav_menus(
		array(
			'primary' => __( 'Main Menu', 'awesome' ),
			'secondary' => __( 'Footer Menu', 'awesome' ),
			)
	);
}

add_action( 'wp_enqueue_scripts', 'awesome_load_scripts' );
function awesome_load_scripts()
{
	wp_enqueue_style( 'newsletter-form-css', get_template_directory_uri() . '/css/newsletter.css', array(), '', false);
	wp_enqueue_style( 'core-css', get_template_directory_uri() . '/css/styles.css', array(), '', false);
	wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/style.css', array(), '', false);
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '', true );
	wp_enqueue_script( 'flexslider-js', get_template_directory_uri() . '/src/flexslider/jquery.flexslider-min.js', array(), '', true );
	wp_enqueue_script( 'addthis-js', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51e8468c6a5794fa');
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/js/scripts.js', array(), '', true );
}

add_action( 'comment_form_before', 'awesome_enqueue_comment_reply_script' );
function awesome_enqueue_comment_reply_script()
{
	if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'the_title', 'awesome_title' );
function awesome_title( $title ) {
	if ( $title == '' ) {
		return '&rarr;';
	} else {
		return $title;
	}
}

add_filter( 'wp_title', 'awesome_filter_wp_title' );
function awesome_filter_wp_title( $title )
{
	return $title . esc_attr( get_bloginfo( 'name' ) );
}

add_action( 'widgets_init', 'awesome_widgets_init' );
function awesome_widgets_init()
{
	register_sidebar( array (
		'name' => __( 'Sidebar Widget Area', 'awesome' ),
		'id' => 'primary-widget-area',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => "</li>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}

function awesome_custom_pings( $comment )
{
	$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}

add_filter( 'get_comments_number', 'awesome_comments_number' );
function awesome_comments_number( $count )
{
	if ( !is_admin() ) {
	global $id;
	$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
		return count( $comments_by_type['comment'] );
	} else {
		return $count;
	}
}

function awesome_post_details()
{
	$author = get_the_author();
    $time = get_the_time('F j, Y', get_the_ID());

    if(is_page()) :
		return '<p class="post-details">Posted by <span>' . $author . '</span> on <span>'. $time .'<span></p>';
	else :
		return '<p class="post-details">Posted by <span>' . $author . '</span> on <span>'. $time .'<span> | '. awesome_comments_number('') .' Comments</p>';
	endif;
}