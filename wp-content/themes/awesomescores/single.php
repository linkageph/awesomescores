<?php get_header(); ?>

<section class="page-contents">
	<div class="container">
		
		<div class="row">
			<div class="col-md-9 single-post page-content">

				<?php
					while(have_posts()) :
						the_post();
						$page_content = get_the_content();
				?>

				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php echo awesome_post_details(); ?>

				<?php
					if($page_content) :
						the_content();
					else :
						echo 'Information coming soon.';
					endif;
				?>

				<!-- Go to www.addthis.com/dashboard to customize your tools -->
				<div class="addthis_sharing_toolbox"></div>
				
				<section class="comments">
					<?php if ( ! post_password_required() ) comments_template( '', true ); ?>	
				</section>
				
				<?php endwhile; ?>

			</div>
			<div class="col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>

	</div>	
</section>

<?php get_footer(); ?>