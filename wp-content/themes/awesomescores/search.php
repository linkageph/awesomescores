<?php get_header(); wp_reset_query(); ?>

<section class="page-contents">
	<div class="container">
		
		<div class="row">
			
			<?php if ( have_posts() ) : ?>

			<div class="col-md-9 blog-page">

				<h1 class="page-title search-title"><?php printf( __( 'Search Results for: %s', 'awesome' ), get_search_query() ); ?></h1>

				<?php
					while(have_posts()) : the_post();
						$post_thumbnail = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
						$content = get_the_excerpt();
				?>

				<div class="blog-entry">
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php echo awesome_post_details(); ?>

					<div class="row">
						<div class="col-md-3">
							<?php if(has_post_thumbnail()) : ?>
								<img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo $post_thumbnail; ?>&w=200&h=200" alt="<?php the_title(); ?> Thumbnail" class="img-responsive">
							<?php else : ?>
								<img src="http://placehold.it/200/62b842/ffffff" class="img-responsive" alt="No thumbnail" title="Thumbnail is not set">
							<?php endif; ?>
						</div>
						<div class="col-md-9">
							<p><?php echo $content; ?></p>
							<a href="<?php the_permalink(); ?>" class="awesome-btn">read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>

				<?php endwhile; ?>

				<?php wp_pagenavi(); ?>

			</div>

			<?php else : ?>

				<div class="col-md-9 page-content">

					<h1 class="page-title"><?php _e('Not Found', 'awesome'); ?></h1>

					<p>It looks like the page you're looking for has been removed or it doesn't exist. Try another search below.</p>
					<?php get_search_form(); ?>

				</div>

			<?php endif; ?>
			
			<div class="col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>

	</div>	
</section>

<?php get_footer(); ?>