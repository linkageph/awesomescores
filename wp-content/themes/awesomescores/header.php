<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php wp_title( ' | ', true, 'right' ); ?></title>
    <link rel="shortcut icon" href="<?php echo get_image('favicon.ico') ?>" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php if(is_single()) : ?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51e8468c6a5794fa"></script>
<?php endif; ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=158036084390781";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- aweber integration -->
<script type="text/javascript" src="http://analytics.aweber.com/js/awt_analytics.js?id=3NVW"></script>

<header>
    
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-sm visible-xs" href="#">Awesome Scores</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="row">
                    <div class="col-md-4">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>
                            <li><a href="<?php echo get_page_link(2); ?>">About Us</a></li>
                            <li><a href="<?php echo get_page_link(5); ?>">Classes</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 text-center visible-lg visible-md">
                        <a href="#" class="logo"><img src="<?php echo get_image('logo.png'); ?>" alt="Awesomescores Logo"></a>
                    </div>
                    <div class="col-md-4">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="<?php echo get_page_link(6); ?>">Blog</a></li>
                            <li><a href="<?php echo get_page_link(7); ?>">Contact Us</a></li>
                            <li><a><div class="fb-like" data-href="https://www.facebook.com/ActHelpCenter" data-width="76" data-layout="button_count" data-show-faces="true" data-send="false"></div></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

</header>