<?php get_header(); ?>

<?php
    $args = array(
        'post_type'         => 'post',
        'cat'               => 3,
        'posts_per_page'    => -1,
        'orderby'           => 'date',
        'order'             => 'ASC'
        );
    $banners = new WP_Query($args);

    if($banners->have_posts()) :
?>

<section class="banner hidden-xs">
    <div class="container">
        <div class="flexslider">
            <ul class="slides">
                <?php
                    while($banners->have_posts()) :
                        $banners->the_post();
                        $featured_image = wp_get_attachment_url(get_post_thumbnail_id($posts->ID));
                ?>
                <li>
                    <?php if(has_post_thumbnail()) : ?>
                        <img src="<?php echo $featured_image; ?>" alt="Banner Image" class="img-responsive">
                        <div class="banner-text text-center">
                            <h1><?php echo get_post_meta(get_the_ID(), 'banner_title', true); ?></h1>
                            <a href="<?php the_permalink(); ?>" class="awesome-btn-lg">View Details <i class="fa fa-angle-right"></i></a>
                        </div>
                    <?php else : ?>
                        <img src="http://placehold.it/1255x656&text=please+set+a+feature+image" alt="No Featured Image">
                    <?php endif; ?>
                </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </div>
</section>

<?php endif; ?>

<section class="newsletter">

    <?php

        $args = array(
            'post_type' => 'page',
            'page_id'   => 13
            );
        $newsletter_section = new WP_Query($args);

    ?>

    <div class="container">
        
        <div class="row">
            <div class="col-md-4 newsletter-form">
                <?php echo get_awesome_template('newsletter'); ?>   
            </div>
            <div class="col-md-8 newsletter-section-text">
                <?php while($newsletter_section->have_posts()) : $newsletter_section->the_post(); ?>
                    <h1><?php the_title(); ?></h1>
                    <p><?php echo page_excerpt(); ?></p>
                    <a href="<?php the_permalink(); ?>" class="awesome-btn">Read More</a>
                <?php endwhile; ?>
            </div>
        </div>
        

    </div>
</section>

<section class="circle-blurbs">
    <div class="container">
    <?php

        $args = array(
            'post_type' => 'post',
            'cat'       => 4,
            'orderby'   => 'date',
            'order'     => 'ASC'
            );
        $courses = new WP_Query($args);

    ?>    
        <h1 class="text-center">Our Tutorials & ACT Prep Courses</h1>

        <div class="row">
            <?php
                while($courses->have_posts()) : $courses->the_post();
                $circle_thumbnail = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            ?>
            <div class="col-md-3 text-center circle-blurb">
                <h3><?php the_title(); ?></h3>
                <?php if($circle_thumbnail) : ?>
                    <img src="<?php echo $circle_thumbnail; ?>" alt="<?php the_title(); ?> icon">
                <?php else : ?>
                    <img src="http://placehold.it/200/62b842/ffffff" class="img-circle" alt="No thumbnail" title="Thumbnail is not set">
                <?php endif; ?>
                <p><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" class="awesome-btn">See Details</a>
            </div>
            <?php endwhile; ?>
        </div>

    </div>
</section>

<section class="call-to-action">
    <div class="container">
        
        <div class="row">
            <div class="col-md-9">
                <h1>Sign-up now to start reaching your goals today!</h1>
            </div>
            <div class="col-md-3">
                <a href="#" class="call-to-action-btn">Get Started Now <i class="fa fa-caret-right"></i></a>
            </div>
        </div>

    </div>
</section>

<section class="pricing">
    <div class="container">
        
        <?php

            $args = array(
                'post_type'         => 'page',
                'post_parent'       => 35,
                'posts_per_page'    => 2,
                'orderby'           => 'date',
                'order'             => 'ASC'
                );
            $classes = new WP_Query($args);
        ?>

        <h1 class="text-center">Schedule and Pricing</h1>

        <div class="row">
            <?php while($classes->have_posts()) : $classes->the_post(); ?>
            <div class="col-md-6 price">
                <div class="wrap-price text-center">
                    <div class="price-head">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="price-body">
                        <h1><?php echo get_post_meta(get_the_ID(), 'class_price', true); ?></h1>
                        <p><?php echo get_post_meta(get_the_ID(), 'short_description', true); ?></p>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="price-footer">View Schedule <i class="fa fa-caret-right"></i></a>                
                </div>
            </div>
            <?php endwhile; ?>
        </div>

    </div>
</section>

<section class="tips">

    <?php

        $args = array(
            'post_type'         => 'post',
            'cat'               => 6,
            'posts_per_page'    => 3,
            'orderby'           => 'date',
            'order'             => 'ASC'
            );
        $tips = new WP_Query($args);

    ?>

    <div class="container">

        <div class="row">
        <?php
            while($tips->have_posts()) : $tips->the_post();
        ?>
            <div class="col-md-4">
                <div class="tip text-center">
                    <h2><?php the_title(); ?></h2>
                    <?php echo awesome_post_details(); ?>
                    <p class="post-content"><?php echo get_the_excerpt(); ?></p>
                    <a href="<?php the_permalink(); ?>" class="awesome-btn pull-right">Read More</a>
                </div>
            </div>
        <?php endwhile; ?>
        </div>

    </div>
</section>

<?php get_footer(); ?>