<?php

	// Template Name: ACT Prep Bundle
	get_header();

	$args = array(
		'post_type' 		=> 'post',
		'cat'				=> 8,
		'posts_per_page'	=> 2,
		'orderby'			=> 'date',
		'order'				=> 'ASC',
		);
	$bundles = new WP_Query($args);

?>

<section class="page-contents">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12 page-content">

				<div class="row schedules">
					<?php
						while($bundles->have_posts()) : $bundles->the_post();
						$price = get_post_meta(get_the_ID(), 'class_price', true);
					?>
						<div class="col-md-6 schedule text-center">
							<h1><?php the_title(); ?> Schedule</h1>
							<?php the_content(); ?>
							<div class="paypal-button">
								<?php echo do_shortcode('[wp_cart_button name="'. get_the_title() .'" price="'. $price .'"]'); ?>
							</div>
						</div>
						
					<?php endwhile; ?>
				</div>

			</div>

		</div>

	</div>	
</section>

<?php get_footer(); ?>