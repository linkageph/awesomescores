<?php
// Template Name: Schedule and Pricing
get_header(); ?>

<section class="page-contents">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12 page-content">

				<section class="pricing">
				        
			        <?php

			            $args = array(
			                'post_type'         => 'page',
			                'post_parent'       => 35,
			                'posts_per_page'    => 2,
			                'orderby'           => 'date',
			                'order'             => 'ASC'
			                );
			            $classes = new WP_Query($args);
			        ?>

			        <h1 class="text-center">Schedule and Pricing</h1>

			        <div class="row">
			            <?php while($classes->have_posts()) : $classes->the_post(); ?>
			            <div class="col-md-6 price">
			                <div class="wrap-price text-center">
			                    <div class="price-head">
			                        <h1><?php the_title(); ?></h1>
			                    </div>
			                    <div class="price-body">
			                        <h1><?php echo get_post_meta(get_the_ID(), 'class_price', true); ?></h1>
			                        <p><?php echo get_post_meta(get_the_ID(), 'short_description', true); ?></p>
			                    </div>
			                    <a href="<?php the_permalink(); ?>" class="price-footer">View Schedule <i class="fa fa-caret-right"></i></a>                
			                </div>
			            </div>
			            <?php endwhile; ?>
			        </div>

				</section>
		</div>

	</div>	
</section>

<?php get_footer(); ?>