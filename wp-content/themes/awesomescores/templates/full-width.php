<?php
// Template Name: Full Width Page
get_header(); ?>

<section class="page-contents">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12 page-content">

				<?php
					while(have_posts()) :
						the_post();
						$page_content = get_the_content();
				?>

				<h1 class="page-title"><?php the_title(); ?></h1>

				<?php
					if($page_content) :
						the_content();
					else :
						echo 'Information coming soon.';
					endif;
				?>

				<?php endwhile; ?>

			</div>

		</div>

	</div>	
</section>

<?php get_footer(); ?>