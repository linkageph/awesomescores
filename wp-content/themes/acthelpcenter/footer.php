<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 */
?>
	</div><!-- #main .wrapper -->
</div><!-- #page -->
<footer id="colophon" role="contentinfo">
    	<div id="footer-widget">
        	<div class="inner">
				<?php if ( is_active_sidebar('footer-widget') ) : ?>
                <ul class="widget-list">
					<?php dynamic_sidebar( 'footer-widget' ); endif; ?>
                </ul>
            </div>
        </div>
    	<div class="inner foo-content">
        	<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_class' => 'one_half' ) ); ?>
			<div class="site-info one_half_last">
				Copyright &copy; 2013 Awesome Scores.  Website by: <a href="http://www.customadesign.com" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/logo-footerwhite.png" alt="Custom A Design" style="vertical-align: middle;" /></a>
			</div><!-- .site-info -->
        </div>
	</footer><!-- #colophon -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-20929716-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php wp_footer(); ?>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
	FB.Event.subscribe('edge.create',
    function(response) {
        window.open('https://www.facebook.com/ActHelpCenter', '_newtab');
    }
);
    twttr.events.bind('follow', function(event) {
       window.open('https://twitter.com/ACTHelpCenter', '_newtab');
    });
  });
  
 
    jQuery('.mslide-2 a').attr('href', 'http://algebraiseasy.com/?page_id=190');
    jQuery('.mslide-3 a').attr('href', 'http://algebraiseasy.com/?page_id=143');
    jQuery('.mslide-4 a').attr('href', 'http://algebraiseasy.com/?page_id=79');
</script>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=507872&mt_adid=126366&v1=&v2=&v3=&s1=&s2=&s3='></script>
</body>
</html>