<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
		<div class="featured-post">
			<?php _e( 'Featured post', 'acthelpcenter' ); ?>
		</div>
		<?php endif; ?>
		<header class="entry-header">
			<?php // the_post_thumbnail(); ?>
			<?php if ( is_single() ) : ?>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php else : ?>
			<h1 class="entry-title">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'acthelpcenter' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h1>
			<?php endif; // is_single() ?>
		</header><!-- .entry-header -->

		<?php if ( is_search() || is_category ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<div class="entry-thumb"><?php if ( has_post_thumbnail() ) : the_post_thumbnail('thumbnail'); endif; ?></div>
            <div class="entry-content"><p><?php the_excerpt(); ?></p></div>
            <p><a href="<?php the_permalink(); ?>" class="readmore"><span>Read More</span></a></p>
            <br clear="all"/>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-content">
        	<div class="entry-thumb"><?php if ( has_post_thumbnail() ) : the_post_thumbnail('medium'); endif; ?></div>
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'acthelpcenter' ) ); ?>
			<?php // wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'acthelpcenter' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>

		<footer class="entry-meta">
			<?php acthelpcenter_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit', 'acthelpcenter' ), '<span class="edit-link">', '</span>' ); ?>
			<?php if ( is_singular() && get_the_author_meta( 'description' ) && is_multi_author() ) : // If a user has filled out their description and this is a multi-author blog, show a bio on their entries. ?>
				<div class="author-info">
					<div class="author-avatar">
						<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'acthelpcenter_author_bio_avatar_size', 68 ) ); ?>
					</div><!-- .author-avatar -->
					<div class="author-description">
						<h2><?php printf( __( 'About %s', 'acthelpcenter' ), get_the_author() ); ?></h2>
						<p><?php the_author_meta( 'description' ); ?></p>
						<div class="author-link">
							<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
								<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'acthelpcenter' ), get_the_author() ); ?>
							</a>
						</div><!-- .author-link	-->
					</div><!-- .author-description -->
				</div><!-- .author-info -->
			<?php endif; ?>
		</footer><!-- .entry-meta -->
	</article><!-- #post -->
