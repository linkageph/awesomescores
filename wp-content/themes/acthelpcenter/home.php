<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>
	<div id="home-section-1">
		<div id="primary" class="site-content">
        	<div id="featured">
            	<?php if ( function_exists( 'meteor_slideshow' ) ) { meteor_slideshow(); } ?>
            </div><!-- #featured -->
            
            <div id="content" role="main">
				<?php $home_post_query = new WP_Query('page_id=13'); ?>
                <?php if ( $home_post_query->have_posts() ) : ?>
        
                    <?php /* Start the Loop */ ?>
                    <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<?php	
                        if ( has_excerpt() ) the_excerpt();
                        else the_content( '' ); ?>
                        
                        <a href="<?php the_permalink(); ?>" class="readmore">Read More</a>
                    <?php endwhile; ?>
                <?php endif; // end have_posts() check 
				wp_reset_query(); ?>
            </div><!-- #content -->
        </div><!-- #primary -->

		<div id="secondary" class="widget-area" role="complementary">
			<?php if ( is_active_sidebar('home-sidebar') ) :	dynamic_sidebar( 'home-sidebar' ); endif; ?>
        </div><!-- #secondary -->
    </div><!-- #home-section-1 -->
    
    <div class="clearfix"></div>
    
    <div id="home-section-2">
    	<div class="inner">
            <h1 class="entry-title">Our Tutorial &amp; ACT Prep Courses</h1>
            <?php
            $courses_query = new WP_Query('cat=4&posts_per_page=4&orderby=date&order=asc');
            while ( $courses_query->have_posts() ) : $courses_query->the_post();
                $i++;
                global $more;
                $more = 0;
                $course_title = ( $custom_title = get_post_meta( get_the_ID(), 'Course Title', true ) ) && '' != $custom_title ? $custom_title : apply_filters( 'the_title', get_the_title() );
                $course_permalink = ( $custom_permalink = get_post_meta( get_the_ID(), 'Course Link', true ) ) && '' != $custom_permalink ? $custom_permalink : get_permalink();
    
                echo '<div class="course' . ( 2 == $i ? ' last' : '' ) . ( 4 == $i ? ' last' : '' ) . '">';
                    if ( ( $course_icon = get_post_meta( get_the_ID(), 'Course Icon', true ) ) && '' != $course_icon )
                        printf( '<img src="%1$s" alt="%2$s" class="icon"/>', esc_attr( $course_icon ), esc_attr( $course_title ) );
                    echo '<div class="course-content"><h3 class="course_header">' . $course_title . '</h3>';
                        
                    if ( has_excerpt() ) the_excerpt();
                    else the_content( '' );
    
                    printf( '<a href="%s" class="readmore">%s </a>', get_page_link(134), __( 'See Details'));
                echo '</div></div> <!-- end .course -->';
            endwhile;
            wp_reset_postdata();
            ?>
        </div><!-- .inner -->
        <div class="sign-up">
        	<div class="inner">
            	<h3>Sign-up now to start reaching your goals today!</h3>
                <a href="<?php echo get_page_link(79)?>" class="button-orange start-btn">Get Started Now</a>
            </div>
        </div>
    </div><!-- #home-section-2 -->
     
    <div id="home-section-3">
    	<div class="inner">
			<?php $home_post_query = new WP_Query('page_id=35'); ?>
            <?php if ( $home_post_query->have_posts() ) : ?>
        
            	<?php /* Start the Loop */ ?>
                <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php	
                        if ( has_excerpt() ) the_excerpt();
                        else the_content( '' ); ?>
                <?php endwhile; ?>
            <?php endif; // end have_posts() check 
			wp_reset_query(); ?>
         </div>
    </div><!-- #home-section-3 -->
    
    <div class="clearfix"></div>
    
    <div id="home-section-4">
    	<div class="inner">
            <h1 class="entry-title">Educational Tips and Advice</h1>
            <h3>Brought to you by Dave Bobbitt</h3>
            <?php
            $blog_query = new WP_Query('cat=6&posts_per_page=3&orderby=date&order=asc');
            while ( $blog_query->have_posts() ) : $blog_query->the_post();
                $j++;
                global $more;
                $more = 0;
    
                echo '<div class="blog' . ( 3 == $j ? ' last' : '' ) . '">';
                    echo '<h3 class="blog-title">' . get_the_title() . '</h3>';
					echo '<p class="entry_meta">Posted by ' . get_the_author() . ' on ' . get_the_date() . ' | ' . get_comments_number() . ' comments</p>'; ?>
					<p><?php if ( has_excerpt() ) the_excerpt();
                        else excerpt(28); ?></p>
    				
                    <?php printf( '<a href="%s" class="readmore">%s </a>', get_permalink(), __( 'Read More'));
                echo '</div> <!-- end .blog -->';
            endwhile;
            wp_reset_postdata();
            ?>
        </div><!-- .inner -->
    </div><!-- #home-section-4 -->
	
<?php get_footer(); ?>