<?php
/**
 * Template Name: Blog Page Template
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">
			<?php $blog_query = new WP_Query('cat=6&posts_per_page=5'); ?>
			<?php while ( $blog_query->have_posts() ) : $blog_query->the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                	<header class="entry-header">
						<h1 class="entry-title">
							<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'acthelpcenter' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
						</h1>
                        <p class="entry_meta">Posted by <?php echo get_the_author() . ' on ' . get_the_date() . ' | ' . get_comments_number() . ' comments'; ?></p>
					</header>
					<div class="entry-thumb"><?php if ( has_post_thumbnail() ) : the_post_thumbnail('thumbnail'); endif; ?></div>
                    <div class="entry-content"><p><?php the_excerpt(); ?></p></div>
                    <p><a href="<?php the_permalink(); ?>" class="readmore"><span>Read More</span></a></p>
                    <br clear="all"/>
                </article>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>