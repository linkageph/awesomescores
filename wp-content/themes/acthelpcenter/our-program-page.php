<?php
// Template Name: Our Program Page Template

$get_programs = new WP_Query (array(
		'cat' => 4,
		'post_per_page' => -1
	));
?>


<?php get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">
			<h1 class="entry-title">Our Program Details</h1>
			<?php while ( $get_programs->have_posts() ) : $get_programs->the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>