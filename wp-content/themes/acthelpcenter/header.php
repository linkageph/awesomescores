<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> style="margin:0 !important">
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=158036084390781";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- aweber integration -->
<script type="text/javascript" src="http://analytics.aweber.com/js/awt_analytics.js?id=3NVW"></script>

<div class="header">
    <div class="inner">
        <hgroup>
            <h1 class="site-title">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                    <img src="<?php bloginfo( 'template_url' ); ?>/images/logo.png" alt="ACT"/>
                </a>
            </h1>
        </hgroup>
        <nav id="site-navigation" class="main-navigation" role="navigation">
            <h3 class="menu-toggle"><?php _e( 'Menu', 'acthelpcenter' ); ?></h3>
            <a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'acthelpcenter' ); ?>"><?php _e( 'Skip to content', 'acthelpcenter' ); ?></a>
            <div class="head-fb-like">
                <div class="fb-like" data-href="https://www.facebook.com/ActHelpCenter" data-width="76" data-layout="button_count" data-show-faces="true" data-send="false"></div>
            </div>
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
        </nav><!-- #site-navigation -->
    </div>
</div>

<div id="page" class="hfeed site" <?php if(is_home()) { echo 'style="max-width:100%"'; } ?>>

	<header id="masthead" class="site-header" role="banner">
        <div class="phone" style="position: relative;">
        	<p>call <strong>888.8MY.MATH</strong></p>
            <a href="https://twitter.com/ACTHelpCenter" class="twitter-follow-button" data-show-count="false" data-lang="en">Follow @ACTHelpCenter</a>
            <!--<div class="fb-like" data-href="https://www.facebook.com/ActHelpCenter" data-width="76" data-layout="button_count" data-show-faces="true" data-send="false" style="position: absolute; top: 50px; right: 0px;"></div>-->
          	
          <div class="fb-share-button" data-href="http://developers.facebook.com/docs/plugins/" data-width="100px" data-type="button_count" style="position: absolute; top: 30px; right: 270px;"></div>
        </div>
	</header><!-- #masthead -->

	<div id="main" class="wrapper">