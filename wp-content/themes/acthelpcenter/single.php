<?php
/**
 * The Template for displaying all single posts.
 *
 */

get_header(); ?>

<div style="margin-top:80px;">
	<div id="primary" class="site-content">
		<div id="content" role="main">

      <h2 class="single-title"><?php the_title(); ?></h2>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php // get_template_part( 'content', get_post_format() ); ?>

				<?php //comments_template( '', true ); ?>
          
          <div class="entry-content">
          
        	<div class="entry-thumb"><?php if ( has_post_thumbnail() ) : the_post_thumbnail('medium'); endif; ?></div>
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'acthelpcenter' ) ); ?>
			<?php // wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'acthelpcenter' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>