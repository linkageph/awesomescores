This plugin is based on all login logo customisation plugins that are available via [wordpress plugins repository](http://wordpress.org/plugins/). I only chose those options that we need.

####Options are:
> 1. Change logo url
> 2. Change the the login logo
> 3. Add custom style
> 4. Change body background

####Instructions:
> 1. Download the plugin [here](https://bitbucket.org/linkageph/cad-login-screen-customisation/get/d475bc8e3252.zip)
> 2. Upload to wordpress plugins/upload or via FTP
> 3. Activate the plugin