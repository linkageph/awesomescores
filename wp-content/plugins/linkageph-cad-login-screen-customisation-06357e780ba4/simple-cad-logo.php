<?php
/**
 * Plugin Name:       CAD Login Screen Customisation (BETA)
 * Plugin URI:        http://customadesign.com
 * Description:       A simple way to customize the wordpress login logo. Based on all login screen customisation plugins out there, all options are optimized so we only get what we need.
 * Version:           1.0.0
 * Author:            Red Deguzman <red.deguzman@customadesign.com>
 * Author URI:        http://twitter.com/redeguzman
 * Bitbucket Plugin URI: https://bitbucket.org/linkageph/cad-login-screen-customisation
 *
 * @package WordPress
 * @author Red Deguzman
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Include plugin class files
require_once( 'includes/class-cad-logo.php' );
require_once( 'includes/class-cad-logo-settings.php' );

/**
 * Returns the main instance of Cad_Logo to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Cad_Logo
 */
function Cad_Logo () {
	$instance = Cad_Logo::instance( __FILE__, '1.0.0' );
	if( is_null( $instance->settings ) ) {
		$instance->settings = Cad_Logo_Settings::instance( $instance );
	}
	return $instance;
}

Cad_Logo();

function cl_login_head()
{	
	$image_url_set = get_option('cl_an_image');
	$logo_url = get_option('cl_logo_url');

	// Logo
	if($image_url_set) :
		$image_url = wp_get_attachment_url(get_option('cl_an_image'));	
	else :
		$image_url = plugins_url('assets/default/customadesign-logo.png', __FILE__);
	endif;

	$styles 	= '<style type="text/css">';
	$styles 	.= 'body { background: ' .get_option('cl_colour_picker'). ' }';
	$styles 	.= 'h1 a {background-image:url('.$image_url.') !important; margin:0 auto;}';
	$styles		.= get_option('cl_custom_css');
	$styles 	.= '</style>';
	echo $styles;

	// URL
	if($logo_url) :
		$logo_url_to =  get_option('cl_logo_url');
	else :
		$logo_url_to = 'http://customadesign.com';
	endif;

	$scripts 	= '<script src="http://code.jquery.com/jquery.min.js"></script>';
	$scripts 	.= '<script type="text/javascript">';
	$scripts 	.= 'jQuery(document).ready(function($){';
	$scripts	.= '$(".login h1 a").attr("href", "' .$logo_url_to. '")';
	$scripts	.= '})';
	$scripts	.= '</script>';
	echo $scripts;

}	

add_filter('login_head', 'cl_login_head' );
