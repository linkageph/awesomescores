<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'custinfo_awesomescores');

/** MySQL database username */
define('DB_USER', 'custinfo_red');

/** MySQL database password */
define('DB_PASSWORD', '123asd654qwe');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'LL$-eDUWOwzV9es~,Ke6nV3wS=VfoE$>:#h3G-8D7Tv EI&kSk*bw+s.oD||Ff|6');
define('SECURE_AUTH_KEY',  '5vH]^W>@v$O>}*%:USu3$XeC|-lp~mZ^RV608|-Yo}H$DMFo8:Qa{hGe|W4#[TH6');
define('LOGGED_IN_KEY',    'u3f0qxoE7@PC-+A*i[h8u9iNtU#o3Sg-:zd;-mGsaL+-]_|Gli]U{:7Tz9 /;9*`');
define('NONCE_KEY',        'cuSy2!4C.~~)bb|KcA`GO~/^z~QW~d4i$5A |.aoVg08O<)/NE)@_q%&S15SqnDB');
define('AUTH_SALT',        'W(cKB2K]*WqRJ5H;sna7eeC!flsek|y- 25!V3a*8?G<NnO?Z~<]#(m1_x1ANrJJ');
define('SECURE_AUTH_SALT', '<twlppV(yIw|mzk?S:b{ID&v&|UrRyGb!W> _ej`Z=QESC8K xk.WP;Io{4+sF-%');
define('LOGGED_IN_SALT',   'B:ax<+A+CyN}rt3_ U8^q|J1CB?%3pWgV8_R<E79pFiB -,zH-@+:8_nC(m2lt)z');
define('NONCE_SALT',       '+3>V:>W;nlKA~*2:1b6xYHcj:pZs0^y^xpx$>+@Xhnc94|G/gvm=$Jb)-ph_e@@|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
